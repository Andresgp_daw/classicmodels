package fp.daw.classicmodels;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/cart")
public class CartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession sesion = request.getSession();
		if (sesion.getAttribute("customer") == null)
			response.sendRedirect("catalogo.jsp");
		else {
			String code = request.getParameter("c");
			String name = request.getParameter("n");
			String priceAux = (request.getParameter("p"));
			if (code == null || name == null || priceAux == null) {
				response.sendRedirect("catalogo.jsp");
			} else {
				double Price = Double.parseDouble(priceAux);
				Map<String, LineaDetalle> cart;
				cart = (Map<String, LineaDetalle>) sesion.getAttribute("cart");
				if (cart == null) {
					cart = new HashMap<>();
					sesion.setAttribute("cart", cart);
				}
				LineaDetalle line = cart.get(code);
				if (line == null)
					cart.put(code, new LineaDetalle(code, name, 1, Price));
				else {
					line.setAmount(line.getAmount() + 1);
					request.getRequestDispatcher("");
				}
				request.getRequestDispatcher("catalogo.jsp").forward(request, response);
			}
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
