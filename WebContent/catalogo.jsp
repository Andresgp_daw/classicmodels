<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<sql:query var="modelos" dataSource="jdbc/classicmodels">
	select productCode, productName, productScale, productDescription, MSRP from products;
</sql:query>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Catálogo de Productos</title>
</head>
<body>
	<h1>Catálogo de Modelos a Escala</h1>
	<hr />
	<c:choose>
		<c:when test="${not empty sessionScope.customer}">
			<p>${sessionScope.customer}</p>
			<p>
				<a href=cart.jsp>Ver Carrito</a>
			</p>
			<p>
				<a href=logout.jsp>Cerrar Sesion</a>
			</p>
		</c:when>
		<c:otherwise>
			<p>
				<a href=login.jsp>Iniciar Sesion</a>
			</p>
			<p>
				<a href=registro.jsp>Registrarse</a>
			</p>
		</c:otherwise>
	</c:choose>
	<c:forEach var="modelo" items="${modelos.rows}">
		<h3>${modelo.productName}</h3>
		<p>Escala: ${modelo.productScale} </p>
		<p>${modelo.productDescription} </p>
		<p>
			Precio: ${modelo.buyPrice}
			<c:if test="${not empty sessionScope.customer }">
				<a href="cart?c=${modelo.productCode}&n=${modelo.productName}&p=${modelo.MSRP}">Comprar
					<img style="width: 1em; height: 1em" src="">
				</a>
			</c:if>
		</p>
		<hr />
	</c:forEach>
</body>
</html>
